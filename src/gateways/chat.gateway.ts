import { SubscribeMessage, WebSocketGateway, OnGatewayInit, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { Controller, Logger } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { PhotosService } from '../photos/services/photos.service';

export class Message { 
  sender: string;
  message: string;
  room: string;
}

@WebSocketGateway({ namespace: '/chat' })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(private photosService: PhotosService) {}
  messages: Message[] = [];
  users: any[] = [];

  handleDisconnect(client: any) {
    this.users = this.users.filter(c => c !== client)

    this.wss.emit('chatToClient', {sender: 'Server', message: `Number of users: ${this.users.length}`});
  }

  handleConnection(client: any, ...args: any[]) {
    this.users.push(client);
    this.wss.emit('chatToClient', {sender: 'Server', message: `Number of users: ${this.users.length}`});
  }

  @WebSocketServer() 
  wss: Server;

  private logger: Logger = new Logger('ChatGateway');

  afterInit(server: any) {
    this.logger.log('Initialized!');
    this.photosService.messages$.subscribe(message => this.wss.emit('chatToClient', message))
  }


  @SubscribeMessage('chatToServer')
  handleMessage(client: Socket, message: Message) {
    this.wss.emit('chatToClient', message);
  }

}
