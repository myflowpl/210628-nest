import {MigrationInterface, QueryRunner} from "typeorm";

export class updatePhotoEntityWithSize1625049340109 implements MigrationInterface {
    name = 'updatePhotoEntityWithSize1625049340109'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_photo_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "filename" varchar NOT NULL, "description" text, "size" integer)`);
        await queryRunner.query(`INSERT INTO "temporary_photo_entity"("id", "filename", "description") SELECT "id", "filename", "description" FROM "photo_entity"`);
        await queryRunner.query(`DROP TABLE "photo_entity"`);
        await queryRunner.query(`ALTER TABLE "temporary_photo_entity" RENAME TO "photo_entity"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "photo_entity" RENAME TO "temporary_photo_entity"`);
        await queryRunner.query(`CREATE TABLE "photo_entity" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "filename" varchar NOT NULL, "description" text)`);
        await queryRunner.query(`INSERT INTO "photo_entity"("id", "filename", "description") SELECT "id", "filename", "description" FROM "temporary_photo_entity"`);
        await queryRunner.query(`DROP TABLE "temporary_photo_entity"`);
    }

}
