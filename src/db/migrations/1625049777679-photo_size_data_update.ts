import { NestFactory } from "@nestjs/core";
import { statSync } from "fs";
import { join } from "path";
import {MigrationInterface, QueryRunner} from "typeorm";
import { ConfigModule, ConfigService } from "../../config";

export class photoSizeDataUpdate1625049777679 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        const app = await NestFactory.createApplicationContext(ConfigModule);
        const config = app.get(ConfigService);

        const photos = await queryRunner.query("SELECT * FROM photo_entity WHERE size is null");
        for (let i = 0; i < photos.length; i++) {
            const photo = photos[i];
            const stat = statSync(join(config.STORAGE_PHOTOS, photo.filename))
            await queryRunner.manager.update('photo_entity', photo.id, { size: stat.size });
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {

    }

}
