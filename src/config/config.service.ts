import { Injectable } from '@nestjs/common';
import { resolve } from 'path';

@Injectable()
export class ConfigService {
  readonly JWT_SECRET = process.env.JWT_SECRET;
  readonly TOKEN_HEADER_NAME = process.env.TOKEN_HEADER_NAME;

  // readonly STORAGE_TMP =  undefined;
  readonly STORAGE_TMP = resolve(__dirname, '../../storage/tmp');
  readonly STORAGE_PHOTOS = resolve(__dirname, '../../storage/photos');
  readonly STORAGE_ASSETS = resolve(__dirname, '../../storage/assets');
  readonly STORAGE_THUMBS = resolve(__dirname, '../../storage/assets/photos');
  readonly PHOTOS_BASE_PATH = '/photos';
  readonly DB_NAME = resolve(__dirname, '../../storage/db.sql');

  readonly ORMCONFIG = {
    "type": "sqlite",
    "database": this.DB_NAME,
    "entities": [
      "src/**/*.entity.ts"
    ],
    "migrationsTableName": "migrations",
    "migrations": ["src/db/migrations/*.ts"],
    "cli": {
      "migrationsDir": "src/db/migrations"
    }
  };



}

