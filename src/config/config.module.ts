import { Injectable, Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { HttpExceptionFilter } from '../shared/http-exception.filter';
import { ConfigService } from './config.service';

@Injectable()
export class DbConnection {}

@Module({
  imports: [],
  providers: [
    DbConnection,
    {
    provide: ConfigService,
    useFactory: async (db: DbConnection) => {
      //todo connect to db and load config
      return new ConfigService()
    },
    inject: [DbConnection]
  },
  {
    provide: APP_FILTER,
    useClass: HttpExceptionFilter,
  }],
  exports: [ConfigService],
})
export class ConfigModule {}
