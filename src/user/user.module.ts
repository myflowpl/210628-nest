import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '../config';
import { UserController } from './controllers/user.controller';
import { logger } from './middlewares/logger.middleware';
import { UserMiddleware } from './middlewares/user.middleware';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as entities from './entities';

const ormModule = TypeOrmModule.forFeature(Object.values(entities));

@Module({
  imports: [ConfigModule, ormModule],
  controllers: [UserController],
  providers: [UserService, AuthService],
  exports: [UserService, ormModule, AuthService]
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(logger, UserMiddleware).forRoutes(UserController);
  }
}
