import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength, IsEmail, IsNumber, IsOptional } from 'class-validator';
import { Exclude, Expose, Transform } from 'class-transformer';
import { UserEntity } from '../entities';

export class UserRegisterRequestDto {
  
  @ApiProperty({example: 'Piotr'})
  name: string;

  @ApiProperty({example: 'piotr@myflow.pl'})
  email: string;

  @ApiProperty({example: '!@#$'})
  password: string;
}

export class UserRegisterResponseDto {
  user: UserEntity;
}

export class UserLoginRequestDto {

  @IsNumber()
  @IsOptional()
  @Exclude({ toPlainOnly: true })
  @Transform(({value}) => parseInt(value, 10))
  allow?: number = 0;

  @IsEmail()
  @ApiProperty({example: 'piotr@myflow.pl'})
  email: string;

  @IsString()
  @MinLength(3)
  @ApiProperty({example: '123'})
  password: string;
}

export class UserLoginResponseDto {
  token: string;
  user: UserEntity;
}


export class GetUserByIdResponseDto {

  @Exclude()
  token: string;
  allow: boolean;
  user: UserEntity; 
  
  constructor(partial: Partial<GetUserByIdResponseDto>) {
    Object.assign(this, partial);
  }
}
