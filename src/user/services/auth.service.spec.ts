import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule, ConfigService } from '../../config';
import { TokenPayloadModel } from '../models';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {

    // const config: Partial<ConfigService> = {JWT_SECRET: 'jwt-secret'};

    const module: TestingModule = await Test.createTestingModule({
      // imports: [ConfigModule],
      providers: [
        AuthService,
        {
          provide: ConfigService,
          useValue: {JWT_SECRET: 'jwt-secret'} as ConfigService
        }
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should encode & decode payload', async () => {
    const payload: TokenPayloadModel = {
      user: {
        id: 1,
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      }
    }
    const token = await service.tokenSign(payload);
    expect(typeof token).toBe('string');
    expect(service.tokenVerify(token)).toMatchObject(payload);
  });
});
