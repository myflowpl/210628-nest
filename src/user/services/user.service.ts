import { Injectable } from '@nestjs/common';
import { UserRegisterRequestDto } from '../dto';
import { UserEntity, UserRoleEntity } from '../entities';
import { UserRole } from '../models';

@Injectable()
export class UserService {


  async findByCredentials(email: string, password: string): Promise<UserEntity> {
    return UserEntity.findOne({ email, password });
  }

  async create(data: UserRegisterRequestDto): Promise<UserEntity> {

    let role: UserRoleEntity = await UserRoleEntity.findOne({ name: UserRole.ADMIN });
    if (!role) {
      role = UserRoleEntity.create({ name: UserRole.ADMIN });
      await UserRoleEntity.save(role);
    }

    const userEntity: UserEntity = UserEntity.create();


    userEntity.name = data.name;
    userEntity.email = data.email;
    userEntity.password = data.password;
    userEntity.roles = [role];

    await UserEntity.save(userEntity);

    return userEntity;

  }

  async getById(id: number): Promise<UserEntity> {
    return UserEntity.findOne(id);
  }


  // users: UserModel[] = [{
  //   id: 1,
  //   name: 'Piotr',
  //   email: 'piotr@myflow.pl',
  //   password: '123',
  //   roles: [UserRole.ADMIN],
  // }];

  // async findByCredentials(email: string, password: string): Promise<UserModel> {
  //   return this.users.find(user => user.email === email && user.password === password);
  // }


  // async create(data: UserRegisterRequestDto): Promise<UserModel> {

  //   const user: UserModel = {
  //     id: this.users.length + 1,
  //     email: data.email,
  //     name: data.name,
  //     password: data.password,
  //     roles: [],
  //   };
  //   this.users.push(user);
  //   return user;
  // }

  // async getById(id: number): Promise<UserModel | null> {

  //   let user = this.users.find(u => u.id === id);

  //   if(user) {
  //     user = new UserModel(user);
  //   }
  //   return user;
  // }

}
