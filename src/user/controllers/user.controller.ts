import { Body, ClassSerializerInterceptor, Controller, Get, Headers, HttpException, HttpStatus, Param, ParseIntPipe, Post, Request, UseFilters, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiParam } from '@nestjs/swagger';
import { identity, Observable } from 'rxjs';
import { ConfigService } from '../../config';
import { HttpExceptionFilter } from '../../shared/http-exception.filter';
import { Roles } from '../decorators/roles.decorator';
import { User } from '../decorators/user.decorator';
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto, GetUserByIdResponseDto } from '../dto';
import { UserEntity } from '../entities';
import { AuthGuard } from '../guards/auth.guard';
import { UserInterceptor } from '../interceptors/user.interceptor';
import { UserRole } from '../models';
import { UserByIdPipe } from '../pipes/user-by-id.pipe';
import { AuthService, UserService } from '../services';
import { ClientProxy, Transport, Client } from '@nestjs/microservices';

@Controller('user')
// @UseFilters(HttpExceptionFilter)
export class UserController {

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Client({ transport: Transport.TCP, options: {port: 3001} })
  client: ClientProxy;

  @Get('sum/:numbers')
  sum(@Param('numbers') numbers: string): Observable<number> {
    const pattern = 'sum';
    const payload = numbers ? numbers.split(',').map(v => parseInt(v, 10)) : [];
    return this.client.send<number>(pattern, payload);
  }

  @Post('login')
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);
    
    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNAUTHORIZED);
    }
  
    return {
      token: await this.authService.tokenSign({ user }),
      user,
    };
  }
  

  // @Post('login')
  // // @UsePipes(new ValidationPipe({ transform: true, }))
  // async login(@Body(new ValidationPipe({})) credentials: UserLoginRequestDto, @Param(ValidationPipe) headers): Promise<UserLoginResponseDto> {
  //   // console.log('Credentials', credentials)
  //   const user = await this.userService.findByCredentials(credentials.email, credentials.password);

  //   if (!user) {
  //     throw new HttpException('Wrong credentials', HttpStatus.UNAUTHORIZED);
  //   }
  //   return {
  //     token: await this.authService.tokenSign({user}),
  //     user,
  //   };
  // }

  @Post('register')
  async register(
    @Body() data: UserRegisterRequestDto,
  ): Promise<UserRegisterResponseDto> {
    const user = await this.userService.create(data);
    // TODO handle errors
    return {
      user,
    };
  }

  @Get()
  @UseGuards(AuthGuard)
  @Roles(UserRole.ADMIN)
  @UseInterceptors(UserInterceptor)
  @ApiBearerAuth()
  async getUser(@User() user: UserEntity) {
    console.log('CONTROLLER: request handler')
    return { user };
  }
  
  @Get(':id')
  @UseGuards(AuthGuard)
  @Roles(UserRole.ADMIN)
  @UseInterceptors(UserInterceptor)
  @ApiBearerAuth()
  @ApiParam({name: 'id', type: Number})
  // @UseInterceptors(ClassSerializerInterceptor)
  async getUserById(@Param('id', UserByIdPipe) user: UserEntity): Promise<GetUserByIdResponseDto> {
    
    // throw new Error('TEST ERROR');

    // throw new HttpException('Wrong credentials', HttpStatus.UNAUTHORIZED);

    console.log('CONTROLLER:', user.id)
    
    return new GetUserByIdResponseDto({ user, allow: true, token: 'sdfsdf' });
  }
  
}

