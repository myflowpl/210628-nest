import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map, tap, delay } from 'rxjs/operators';

@Injectable()
export class UserInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {

    const req = context.switchToHttp().getRequest();
    const res = context.switchToHttp().getResponse();

    console.log('INTERCEPTOR: BEFORE')
    console.time('intercept');

    return next.handle().pipe(
      map(response => response),
      delay(1000),
      tap(user => console.log('INTERCEPTOR: AFTER')),
      tap(user => console.timeEnd('intercept'))
    );

  }
}
