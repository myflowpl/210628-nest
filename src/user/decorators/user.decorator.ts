import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export function userDecoratorFactory(data: unknown, ctx: ExecutionContext) {

  const req = ctx.switchToHttp().getRequest();

  return req.tokenPayload && req.tokenPayload.user
    ? req.tokenPayload.user
    : undefined;
}

export const User = createParamDecorator(userDecoratorFactory);
