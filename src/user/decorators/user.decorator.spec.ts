import { ExecutionContext } from "@nestjs/common"
import { userDecoratorFactory } from "./user.decorator"

describe('User Decorator', () => {
  const req = {
    tokenPayload: {
      user: {id: 1, name: 'Piotr'}
    }
  }
  const ctx = {
    switchToHttp: () => ({
      getRequest: () => req
    })
  } as ExecutionContext;

  it('should return user', () => {
    expect(userDecoratorFactory({}, ctx)).toMatchObject(req.tokenPayload.user);
  })
})