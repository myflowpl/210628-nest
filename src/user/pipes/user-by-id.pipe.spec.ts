import { BadRequestException, NotFoundException } from '@nestjs/common';
import { UserEntity } from '../entities';
import { UserService } from '../services';
import { UserByIdPipe } from './user-by-id.pipe';

describe('UserByIdPipe', () => {
  let pipe: UserByIdPipe;

  const user:any = {
    id: 1,
    name: 'Piotr'
  }

  const userServiceMock = {
    async getById(id: number): Promise<UserEntity> {
      return (id === 1) ? user : null;
    }
  } as UserService;

  beforeAll(() => {
    pipe = new UserByIdPipe(userServiceMock);
  });

  it('should be defined', () => {
    expect(pipe).toBeDefined();
  });

  it('should throw NotFoundException', async () => {
    return expect(pipe.transform('2')).rejects.toThrow(NotFoundException);
  });

  it('should return User', () => {
    return expect(pipe.transform('1')).resolves.toMatchObject(user);
  });

  it('should throw BadRequestException', () => {
    return expect(pipe.transform('sdfdf')).rejects.toThrow(BadRequestException);
  });
});
