import { Exclude } from "class-transformer";
import { UserEntity } from "../entities";

export enum UserRole {
  ADMIN = 'admin',
  ROOT = 'root',
}

// export class UserModel {
  
//   constructor(partial: Partial<UserModel>) {
//     Object.assign(this, partial);
//   }

//   id?: number;
//   name: string;
//   email?: string;

//   @Exclude({toPlainOnly: true})
//   password?: string;

//   roles?: UserRole[];
// }

export class TokenPayloadModel {
  user: UserEntity;
}
