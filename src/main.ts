require('dotenv').config();

import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { expressApp } from '../express/server';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import { CommentsController } from './comments/controllers/comments.controller';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ConfigService } from './config';
import { HttpExceptionFilter } from './shared/http-exception.filter';
import { join } from 'path';
import { ClassSerializerInterceptor } from '@nestjs/common';
import { Transport } from "@nestjs/microservices";
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter(expressApp));
  
  const microservice = app.connectMicroservice({
    transport: Transport.TCP,
  });
  
  await app.startAllMicroservicesAsync();

  app.setGlobalPrefix('api');
  app.enableShutdownHooks();
  app.enableCors();

  app.setBaseViewsDir(join(__dirname, 'views'));
  app.setViewEngine('hbs');
  
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)))
  
  // const ctrl = app.get(CommentsController);
  // console.log(ctrl.getComments({search: 'p'}))

  // const filter = app.get(HttpExceptionFilter);
  // app.useGlobalFilters(filter)

  // const config = new ConfigService();
  const config = app.get(ConfigService);
  const options = new DocumentBuilder()
    .setTitle('Nest API Example')
    .setDescription('Przykładowy projekt w Node.js i TypeScript')
    .setVersion('1.0')
    .addTag('user')
    .addBearerAuth({type: 'apiKey', in: 'header', name: config.TOKEN_HEADER_NAME})
    .build();
  const document = SwaggerModule.createDocument(app, options);
  // console.log(document); dostepny na /api/docs-json
  SwaggerModule.setup('api/docs', app, document, {
    swaggerOptions: {
      persistAuthorization: true,
    },
  });

  await app.listen(3000);
}
bootstrap();
