import { existsSync, readFileSync, writeFileSync } from "fs";
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const inquirer = require('inquirer');

(async function setup() {
  const envFile = './.env';
  const envTplFile = './.env-tpl';
  if(existsSync(envFile)) {
    return console.log(chalk.green('This project is ready :)'))
  }
  console.log(chalk.green(figlet.textSync('Setting up..')))
  let tpl = readFileSync(envTplFile).toString();

  const data = await inquirer.prompt([
    {
      type: 'text',
      name: 'jwt_secret',
      message: 'Select your jwt secret',
      default: 'jwt-secret',
      validate: (v) => !!v
    },
    {
      type: 'password',
      name: 'db_password',
      message: "set your db password",
    }
  ]);
  for (const key in data) {
    if (Object.prototype.hasOwnProperty.call(data, key)) {
      const value = data[key];
      tpl = tpl.replace(`<${key}>`, value);
    }
  }
  writeFileSync(envFile, tpl);
  console.log(chalk.green('Your project is ready ;)'))
})()