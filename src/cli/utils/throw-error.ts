const chalk2 = require('chalk');

module.exports.throwError = function(message) {
  console.log(chalk.red('------'));
  console.error(chalk.red('ERROR:'), message);
  console.log(chalk.red('------'));
  throw new Error(message);
}
