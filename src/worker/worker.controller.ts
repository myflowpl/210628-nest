import { Controller, HttpModule } from '@nestjs/common';
import { MessagePattern, EventPattern, Client, ClientProxy, Transport } from '@nestjs/microservices';
import { PhotosService } from '../photos/services/photos.service';

@Controller()
export class WorkerController {

  @Client({ transport: Transport.TCP, options: {port: 3000} })
  client: ClientProxy;

  constructor(
    private photoService: PhotosService,
    // privite http: HttpModule
  ) {}

  @MessagePattern('sum')
  async accumulate(data: number[]): Promise<number> {
    return (data || []).reduce((a, b) => a + b);
  }

  @MessagePattern('create_thumb')
  async createThumb(filename: string) {
    console.log('Message: create_thumb')
    return this.photoService.createThumbs(filename);
  }

  @EventPattern('create_thumb_event')
  async handleUserCreated(filename: string) {
    console.log('Event: create_thumb_event')
    const res = await this.photoService.createThumbs(filename);
    return this.client.emit('create_thumb_success', res);
  }
}
