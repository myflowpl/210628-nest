import { Injectable, OnApplicationShutdown, OnModuleInit } from '@nestjs/common';
import { promisify } from 'util';
import { rename, mkdirSync } from 'fs';
import { extname, join } from 'path';
const renameAsync = promisify(rename);
import * as crypto from 'crypto';
import { ConfigService } from '../../config';
import { resolve } from 'path';
import * as sharp from 'sharp';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PhotoEntity } from '../entities';
import { UserEntity } from '../../user/entities';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class PhotosService implements OnModuleInit, OnApplicationShutdown {

  private messages$$ = new ReplaySubject(5);
  messages$ = this.messages$$.asObservable();

  sendMessage(message) {
    this.messages$$.next(message);
  }

  async onModuleInit() {
    // console.log(`PHOTOS module INIT`);

    if(!this.config.STORAGE_PHOTOS) {
      throw new Error('configService.STORAGE_PHOTOS is required')
    }
    if(!this.config.STORAGE_TMP) {
      throw new Error('configService.STORAGE_TMP is required')
    }

    mkdirSync(this.config.STORAGE_TMP, {recursive: true});
    mkdirSync(this.config.STORAGE_PHOTOS, {recursive: true});
    mkdirSync(this.config.STORAGE_ASSETS, {recursive: true});
    mkdirSync(this.config.STORAGE_THUMBS, {recursive: true});

    // return new Promise((resolve, reject) => {
    //   setTimeout(() => {
    //     console.log('MODULE INIT READY')
    //     // resolve(null)
    //     reject('brak katalogu storage')
    //   }, 2000);
    // })
  }

  async onApplicationShutdown(signal: string) {
    // console.log('SHUTDOWN START', signal); // e.g. "SIGINT"
    // return new Promise((resolve) => {
    //   setTimeout(() => {
    //     console.log('SHUTDOWN READY');
    //     resolve(null)
    //   }, 2000);
    // })
  }

  constructor(
    private config: ConfigService,

    @InjectRepository(PhotoEntity)
    private readonly photoRepository: Repository<PhotoEntity>,

    ) {}

  async create(file: Express.Multer.File, user: UserEntity) {

    // TODO validate is photo

    const fileName = crypto
    .createHash('md5')
    .update(file.path)
    .digest('hex') + extname(file.originalname).toLowerCase();

    await renameAsync(file.path, join(this.config.STORAGE_PHOTOS, fileName));

    const photo = new PhotoEntity();
    photo.filename = fileName;
    photo.size = file.size;
    photo.description = file.originalname;
    photo.user = user;
    await this.photoRepository.save(photo);

    return {
      fileName,
      photo,
    };
  }

  async findAll() {
    return this.photoRepository.find();
  }

  async createThumbs(filename) {

    const sourceFile = resolve(this.config.STORAGE_PHOTOS, filename);
    const destFile = resolve(this.config.STORAGE_THUMBS, filename);

    await sharp(sourceFile)
      .rotate()
      .resize(200, 200, { fit: 'cover', position: 'attention' })
      .jpeg({
        quality: 100,
      })
      .toFile(destFile);

    return {
      thumbName: destFile,
    };
  }

}
