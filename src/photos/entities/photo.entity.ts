import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, BaseEntity } from 'typeorm';
import { UserEntity } from '../../user/entities';

@Entity()
export class PhotoEntity extends BaseEntity {
  
  @PrimaryGeneratedColumn() id: number;

  @Column() filename: string;

  @Column({nullable: true}) size: number;

  @Column('text', {nullable: true}) description: string;
  
  @ManyToOne(type => UserEntity, {eager: true})
  user: UserEntity;
}
 