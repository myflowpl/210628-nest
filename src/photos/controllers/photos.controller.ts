import { Controller, Post, UseInterceptors, Body, UploadedFile, Get, UseGuards, Param, Res, NotFoundException, ForbiddenException } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiProperty } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { PhotosService } from '../services/photos.service';
import { AuthGuard } from '../../user/guards/auth.guard';
import { User } from '../../user/decorators/user.decorator';
import { UserEntity } from '../../user/entities';
import { join } from 'path';
import { Response } from 'express';
import { ConfigService } from '../../config';
import { PhotoEntity } from '../entities';
import { ClientProxy, Transport, Client, EventPattern } from '@nestjs/microservices';

export class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any;
}

@Controller('photos')
export class PhotosController {

  @Client({ transport: Transport.TCP, options: {port: 3001} })
  client: ClientProxy;

  constructor(
    private photosService: PhotosService,
    private configService: ConfigService,
    ) {}

  @Post('upload-user-avatar')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Upload user avatar',
    type: FileUploadDto,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Body() body, @User() user: UserEntity) {

    const avatar = await this.photosService.create(file, user);
    // const avatar = {};
    // const thumb = await this.photosService.createThumbs(avatar.fileName);
    // const thumb = await this.client.send<any>('create_thumb', avatar.fileName).toPromise();

    this.client.send<any>('create_thumb', avatar.fileName).subscribe(
      data => this.photosService.sendMessage({sender: 'Microservice', message: data.thumbName}),
      (err) => console.error(err),
      () => console.log('CREATE THUMB COMPLETE')

    );

    // this.client.emit<any>('create_thumb_event', avatar.fileName);
    const thumb = null;

    return {avatar, thumb, file, body};

  }

  @EventPattern('create_thumb_success')
  createThumbSuccess(data) {
    console.log('handle in controller create_thumb_success', data)
    this.photosService.sendMessage({sender: 'Microservice', message: "created: " + data.thumbName})
  }

  @Get()
  photos() {
    return this.photosService.findAll();
  }

  @Get('download/:fileName')
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  async download(@Param('fileName') filename: string, @Res() res: Response, @User() user: UserEntity) {

    const photo = await PhotoEntity.findOne({filename});

    if(!photo) {
      throw new NotFoundException('Photo not found: '+filename);
    }

    if (!photo.user || photo.user.id !== user.id) {
      throw new ForbiddenException("Sorry! You can't see that.")
    }

    const file = join(this.configService.STORAGE_PHOTOS, filename)

    res.download(file, filename, function (err) {
      if (err) {
        // Handle error, but keep in mind the response may be partially-sent
        // so check res.headersSent
      } else {
        // decrement a download credit, etc.
      }
    })
    
    // jeśli nie chcesz by przeglądarka zrobiła prompt na download to użyj: res.sendFile()
  }
}
