import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigModule, ConfigService } from '../config';
import { PhotosController } from './controllers/photos.controller';
import { PhotosService } from './services/photos.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as entities from './entities';
import { UserModule } from '../user/user.module';
import { DbModule } from '../db/db.module';

@Module({
  imports: [
    UserModule,
    DbModule,
    ConfigModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        dest: config.STORAGE_TMP,
      }),
      inject: [ConfigService],
    }),
    ServeStaticModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ([{
          rootPath: config.STORAGE_ASSETS,
      }])
    }),
    TypeOrmModule.forFeature(Object.values(entities)),
  ],
  controllers: [PhotosController],
  providers: [PhotosService],
  exports: [PhotosService]
})
export class PhotosModule {}
