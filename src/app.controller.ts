import { Body, Controller, Get, Post, Render } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @Render('index')
  getHello() {
    const message = this.appService.getHello();
    const data = { message };
    return {
      view: 'index2',
      data
    }
  }

  @Post('info')
  info(@Body() data) {
    return {
      id: Date.now(),
      data
    };
  }
}
