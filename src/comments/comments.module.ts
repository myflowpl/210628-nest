import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { CommentsController } from './controllers/comments.controller';
import { Logger } from './Logger';
import { CommentsService } from './services/comments.service';

@Module({
  imports: [UserModule],
  controllers: [CommentsController],
  providers: [
    CommentsService, 
    {
      provide: Logger,
      useValue: process.env.NODE_ENV === 'production' ? (v) => { console.log('VAL', v)} : () => {} 
    }
  ]
})
export class CommentsModule {}
