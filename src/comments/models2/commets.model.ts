import { ApiProperty } from "@nestjs/swagger";

export class CommentModel {
  id: number;
  @ApiProperty({
    description: 'Nazwisko komentującego',
    example: 'Piotr',
  })
  name: string;
}
