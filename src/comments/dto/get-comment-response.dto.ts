import { CommentModel } from '../models2';


export class GetCommentResponseDto {
  total: number;
  data: CommentModel;
}
