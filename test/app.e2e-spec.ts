import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
 
describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  it('/info (POST)', () => {

    const data = {name: 'Piotr'};

    const res = {
      id: expect.any(Number),
      data
    }
    
    return request(app.getHttpServer())
      .post('/info')
      .send(data)
      .expect(201)
      .then(r => {
        expect(r.body).toMatchObject(res);
      });
  });
});
