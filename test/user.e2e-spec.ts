require('dotenv').config();
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { UserLoginRequestDto, UserLoginResponseDto, UserRegisterRequestDto, UserRegisterResponseDto } from '../src/user/dto';
import { ConfigService } from '../src/config';
import { AuthService } from '../src/user/services';
import { TokenPayloadModel, UserRole } from '../src/user/models';
 
describe('UserController (e2e)', () => {
  let app: INestApplication;
  let config: ConfigService;
  let authService: AuthService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
    // .overrideProvider(ConfigService)
    // .useValue({JWT_SECRET: 'jwt-secret'} as ConfigService)
    .compile();

    app = moduleFixture.createNestApplication();
    config = app.get(ConfigService);
    authService = app.get(AuthService);

    await app.init();
  });

  it('/user/register (POST)', () => {

    const req: UserRegisterRequestDto = {
      name: 'Piotr',
      email: 'piotr@myflow.pl',
      password: '123',
    };

    const res: UserRegisterResponseDto = {
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      }
    }
    
    return request(app.getHttpServer())
      .post('/user/register')
      .send(req)
      .expect(201)
      .then(r => {
        expect(r.body).toMatchObject(res);
      });
  });

  it('/user/login (POST)', () => {

    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '123',
    };

    const res: UserLoginResponseDto = {
      token: expect.any(String),
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      }
    }
    
    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .then(r => {
        expect(r.body).toMatchObject(res);
      });
  });

  it('/user (GET)', () => {

    const tokenPayload: TokenPayloadModel = {
      user: {
        id: 1,
        name: 'Piotr',
        email: 'piotr@myflow.pl',
        roles: [UserRole.ADMIN]
      }
    }
    const res = {
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
        roles: [UserRole.ADMIN]
      }
    }
    const token = authService.tokenSign(tokenPayload);

    return request(app.getHttpServer())
      .get('/user')
      .set(config.TOKEN_HEADER_NAME, token)
      .then(r => {
        expect(r.body).toMatchObject(res);
      });
  });

  it('/user (GET) Forbidden', () => {

    return request(app.getHttpServer())
      .get('/user')
      .then(r => {
        expect(r.body.statusCode).toBe(403);
      });
  });
});
